﻿using System;

namespace dotnetcore
{
    internal static class ConsoleVisualization
    {
        internal static void ConsolePrint(string text)
        {
            Console.WriteLine(text);
        } 
        internal static int GetUserInput()
        {
            var userInput = 0;
            var flag = true;
            while (flag)
            {
                flag = false;
                try
                {
                    Console.Write("Введите выбранный номер: ");
                    userInput = Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    Console.WriteLine("\nИспользуйте целые числа");
                    flag = true;
                }
            }
            return userInput;
        }
    }
}